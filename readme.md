# Java Program #2

**Student:** Liam Hockley

**Submitted Date:** Feb 27, 2012 9:26 am

**Grade:** 35.0 (max 35.0)

**Instructions**
> Using JOptionPane dialog boxes (may have to research some code on the web) create a Java program using OOP concepts to perform the following:
> 
> - The Moon is spiraling away from the Earth at a rate of 3.8 cm year.
> 
> Develop a program that determines how far away from Earth the Moon will be in the following years:
> 
> 2015, 2025, 2038, 2045, 2052, 2075, 3000
> 
> then find the year that tells us when the Moon will be farther than Mars at it's closest > point to the Earth around 35 million miles.
> 
> You will create a class - call it Moon and then develop the variables and methods you will need to create this program. Then develop an implementation class containing the Main method and execute the program using the class interface. The way it executes is up to you - create an interactive program if you like.
> 
> Use this as the distance of the Moon from the Earth:
> 
> private final double distance = 252000; //average distance moon is from earth in miles
