package moonDist;

public class Moon_Loc {
	//NOTES
	/*
	 * 160934.4 cm = 1 mile
	 * 252000 = Distance of Moon from Earth (Miles)
	 * 252000*160934.4 = Distance of Moon from Earth (cm)
	 * {num}/160934.4 = Distance in Miles where {num} is distance in cm.
	 */
	
	//define vars
	private int year;
	private double a; //Calculation Var FOR HowFar()
	private double b; //Calculation Var FOR HowFar()
	private double c; //Calculation Var FOR HowFar()
	
	Moon_Loc(int y){
		year=y;
	}
	
	//return the Year (in string format)
	public String strGetYear(){
		return Integer.toString(year);
	}
	
	//return the distance of moon from Earth in Miles (calculated in CM)
	public double HowFar(){
		//a little bit of cryptic math :D
		a = year-2012; //find out how many additional years beyond our base year of 2012
		b = a*3.8; //calculate the distance in CM for the additional years
		c = ((252000*160934.4)+b)/160934.4; //Distance in Miles for the given year
		return c;
	}
	
	private boolean EarthProximity(){
		if(HowFar()>35000000){return false;}
		else {return true;} 
	}
	
	public String strLunarProximity(){
		if (!EarthProximity()){return "The Moon is Closer to Mars, than Earth";}
		else {return "The Moon is closer to Earth, than Mars";}
	}
	
			
}
