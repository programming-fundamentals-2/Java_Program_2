package moonDist;

import java.text.DecimalFormat;
import javax.swing.JOptionPane;

public class Moon {

	public static void main(String[] args) {
		//define vars
		
		//decimal format
		DecimalFormat decimal = new DecimalFormat("#.##");
		
		//begin...
		// create moon_loc objects
		Moon_Loc location1 = new Moon_Loc(2012); //default
		Moon_Loc location2 = new Moon_Loc(2015);
		Moon_Loc location3 = new Moon_Loc(3025);
		Moon_Loc location4 = new Moon_Loc(32038);
		Moon_Loc location5 = new Moon_Loc(552045);
		Moon_Loc location6 = new Moon_Loc(1002052);
		Moon_Loc location7 = new Moon_Loc(8002075);
		Moon_Loc location8 = new Moon_Loc(999999999);
				
		
		//create info pane
		JOptionPane.showMessageDialog (null,"It is the year: " +location1.strGetYear()+ " The moon is " + decimal.format(location1.HowFar()) + " miles from earth.\n" 
				+ location1.strLunarProximity()+ "\nLETS DO THE TIME WARP!");
		
		//create info pane
		JOptionPane.showMessageDialog (null,"It is the year: " +location2.strGetYear()+ " The moon is " + decimal.format(location2.HowFar()) + " miles from earth.\n" 
				+ location2.strLunarProximity()+ "\nLETS DO THE TIME WARP AGAIN!");

		//create info pane
		JOptionPane.showMessageDialog (null,"It is the year: " +location3.strGetYear()+ " The moon is " + decimal.format(location3.HowFar()) + " miles from earth.\n" 
				+ location3.strLunarProximity()+ "\nLETS DO THE TIME WARP AGAIN!");
		
		//create info pane
		JOptionPane.showMessageDialog (null,"It is the year: " +location4.strGetYear()+ " The moon is " + decimal.format(location4.HowFar()) + " miles from earth.\n" 
				+ location4.strLunarProximity()+ "\nLETS DO THE TIME WARP AGAIN!");
		
		//create info pane
		JOptionPane.showMessageDialog (null,"It is the year: " +location5.strGetYear()+ " The moon is " + decimal.format(location5.HowFar()) + " miles from earth.\n" 
				+ location5.strLunarProximity()+ "\nLETS DO THE TIME WARP AGAIN!");
		
		//create info pane
		JOptionPane.showMessageDialog (null,"It is the year: " +location6.strGetYear()+ " The moon is " + decimal.format(location6.HowFar()) + " miles from earth.\n" 
				+ location6.strLunarProximity()+ "\nLETS DO THE TIME WARP AGAIN!");
		
		//create info pane
		JOptionPane.showMessageDialog (null,"It is the year: " +location7.strGetYear()+ " The moon is " + decimal.format(location7.HowFar()) + " miles from earth.\n" 
				+ location7.strLunarProximity()+ "\nLETS DO THE TIME WARP AGAIN!");
		
		//create info pane
		JOptionPane.showMessageDialog (null,"It is the year: " +location8.strGetYear()+ " The moon is " + decimal.format(location8.HowFar()) + " miles from earth.\n" 
				+ location8.strLunarProximity()+ "\nLETS DO THE TIME WARP AGAIN!");
		
	}

}
